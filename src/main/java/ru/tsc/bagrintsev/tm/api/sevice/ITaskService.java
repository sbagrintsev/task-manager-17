package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;
import java.util.Map;

public interface ITaskService {

    Task create(String name) throws NameIsEmptyException;

    Task create(String name, String description) throws AbstractFieldException;

    Task add(Task task) throws TaskNotFoundException;

    List<Task> findAll();

    List<Task> findAll(Sort sort);

    Map<Integer, Task> findAllByProjectId(String projectId) throws IdIsEmptyException;

    Task findOneByIndex(Integer index) throws IncorrectIndexException;

    Task findOneById(String id) throws IdIsEmptyException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

    Task updateById(String id, String name, String description) throws AbstractException;

    Task removeTaskByIndex(Integer index) throws AbstractException;

    Task removeTaskById(String id) throws AbstractException;

    Task remove(Task task) throws TaskNotFoundException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractException;

    Task changeTaskStatusById(String id, Status status) throws AbstractException;

    void clear();

}
