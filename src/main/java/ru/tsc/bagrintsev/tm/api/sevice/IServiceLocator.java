package ru.tsc.bagrintsev.tm.api.sevice;

public interface IServiceLocator {

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();
}
