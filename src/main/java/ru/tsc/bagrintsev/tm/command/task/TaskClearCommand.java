package ru.tsc.bagrintsev.tm.command.task;

public class TaskClearCommand extends AbstractTaskCommand {
    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        getTaskService().clear();
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }
}
