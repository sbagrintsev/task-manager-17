package ru.tsc.bagrintsev.tm.exception.system;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException(String arg) {
        super(String.format("Error! Command '%s' is not supported...", arg));
    }

}
