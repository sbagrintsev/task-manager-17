package ru.tsc.bagrintsev.tm.command.project;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        getProjectService().clear();
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

}
