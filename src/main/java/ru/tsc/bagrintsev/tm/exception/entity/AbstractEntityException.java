package ru.tsc.bagrintsev.tm.exception.entity;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class AbstractEntityException extends AbstractException {

    public AbstractEntityException() {
        super();
    }

    public AbstractEntityException(String message) {
        super(message);
    }

    public AbstractEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(Throwable cause) {
        super(cause);
    }

    protected AbstractEntityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
