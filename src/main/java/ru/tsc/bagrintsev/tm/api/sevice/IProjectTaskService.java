package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId) throws AbstractException;

    void unbindTaskFromProject(String projectId, String taskId) throws AbstractException;

    void removeProjectById(String projectId) throws AbstractException;

}
