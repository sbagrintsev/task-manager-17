package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name) throws NameIsEmptyException;

    Project create(String name, String description) throws AbstractFieldException;

    Project add(Project project) throws ProjectNotFoundException;

    List<Project> findAll();

    List<Project> findAll(Sort sort);

    Project findOneByIndex(Integer index) throws IncorrectIndexException;

    Project findOneById(String id) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project removeProjectByIndex(Integer index) throws AbstractException;

    Project removeProjectById(String id) throws AbstractException;

    Project remove(Project project) throws ProjectNotFoundException;

    Project changeProjectStatusByIndex(Integer index, Status stauts) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

    void clear();

}
