package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        getTaskService().updateById(id, name, description);
    }

    @Override
    public String getName() {
        return "task-update-by-id";
    }

    @Override
    public String getDescription() {
        return "Update task by id.";
    }
}
